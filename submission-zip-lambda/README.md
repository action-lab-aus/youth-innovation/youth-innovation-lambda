# Limitless Submission Zip Lambda

The purpose of this lambda function is to allow clients to specify a list of S3 assets to zip together and upload that zip to the S3.

## Building, Testing and Deploying

Deploying this project requires the installation of the AWS CLI. To deploy to AWS Lambda, from the 'submission-zip-lambda' directory:

```
zip -r function.zip .
```

and then

```
aws lambda update-function-code --function-name YouthInnovationSubmissionZip --zip-file fileb://./function.zip
```
