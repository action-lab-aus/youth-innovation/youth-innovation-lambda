const AWS = require('aws-sdk');
const archiver = require('archiver');
const path = require('path');
const admin = require('firebase-admin');

var serviceAccount = require('./firebase-config.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

// Set AWs Credentials from environment
AWS.config.update({
  accessKeyId: 'AKIA4NOSAVB22PY2K2XO',
  secretAccessKey: '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',
  region: 'ap-southeast-2',
});

const S3 = new AWS.S3();
const BUCKET_NAME = 'youth-innovation';

const db = admin.firestore();

/**
 * Iterate over the S3 paths in 'paths' and append them to the archive.
 *
 * Example:
 * Calling the function with:
 * paths = ['/submissions/some_submission_id/some_media_id/asset_name.mp4']
 * and a valid archiver archive will get the file stream for asset_name.mp4 from
 * S3 and add it to the archive stream.
 *
 * @param {archiver.Archiver} archive - The archiver to append the streams to
 * @param {string[]} paths - The list of S3 paths to make the streams from
 */
const appendS3PathsTo = (archive, paths) => {
  paths.forEach(el => {
    console.log(`Appending file: ${el} (${path.basename(el)})`);
    archive.append(S3.getObject({ Bucket: BUCKET_NAME, Key: el }).createReadStream(), { name: path.basename(el) });
  });
};

/**
 * A helper function to check whether a given type of asset should be included in the archive
 *
 * @param {string} type - The type of asset
 * @returns {boolean} Whether that asset type should be included in the archive
 */
const shouldExport = type =>
  type === 'hd_video' || type === 'transcription' || type === 'square_video' || type === 'image';

exports.handler = async event => {
  const body = event.body ? JSON.parse(event.body) : { key: 'No Key Provided' };

  console.log(event);

  if (event.requestContext.http.method === 'OPTIONS') {
    return {
      statusCode: 200,
      headers: {
        'Access-Control-Allow-Headers': 'content-type',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'POST',
        'Access-Control-Max-Age': 600,
      },
    };
  }

  if (body.key !== process.env.SECRET) {
    return {
      statusCode: 400,
      body: JSON.stringify({ error: 'Invalid Secret Key' }),
      headers: {
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'OPTIONS,POST,GET',
      },
    };
  }

  const submissionId = body.submissionId;

  try {
    const submission = (await db.collection('submissions').doc(submissionId).get()).data();

    console.log(submission);

    const media = await Promise.all(submission.media.map(el => el.get())).then(mediaArray =>
      mediaArray.filter(el => shouldExport(el.data().type)),
    );
    const S3Paths = media.map(el => el.data().src.split('/').slice(3).join('/'));

    // Archiver provides a convenient way of creating a zip stream
    const archive = archiver('zip', {
      // Set the level of compression
      zlib: { level: 9 },
    });

    // Log any warnings/errors that occur during compression
    archive.on('warning', er => {
      console.log(er);
    });
    archive.on('error', er => {
      console.log(er);
    });

    // Append the file streams to the archive stream
    appendS3PathsTo(archive, S3Paths);
    // Indicate that there are no more files to add to the archive
    archive.finalize();

    // Upload the archive to S3. The 'Key' is the path in the bucket to the output file
    const filePath = `submissions/${submissionId}/${submission.formdata.title} Media Files.zip`;

    const uploadedfile = await S3.upload({
      Bucket: BUCKET_NAME,
      Key: filePath,
      Body: archive,
    }).promise();

    return {
      statusCode: 200,
      body: JSON.stringify({
        url: uploadedfile.Location,
      }),
      headers: {
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'OPTIONS,POST,GET',
      },
    };
  } catch (e) {
    console.log(e);
  }
};
