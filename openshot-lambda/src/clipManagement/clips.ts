import { AxiosResponse } from 'axios';
import { OpenshotClip } from '../types';

const { default: axios } = require('axios');
const { openshotAuth } = require('../util');

/**
 * Add Clip to Project
 *
 * @param {String} projectUrl - the url of the project the clip is being added to
 * @param {String} fileUrl - the url of the file to be added as a clip
 * @param {object} clipSettings - the settings for the clip. Possible settings are:
 * position: position in the track to place the track
 * start: where within the file to start the video (for cropping)
 * end: where within the file to end the video (for cropping)
 * layer: the layer to place the file on
 * @returns {Promise<AxiosResponse<object>>} the promise for the request
 */
exports.addClipToProject = (
  projectUrl: string,
  fileUrl: string,
  clipSettings,
): Promise<AxiosResponse<OpenshotClip>> => {
  console.log(`Adding clip to project: ${projectUrl}, ${fileUrl}. clipSettings:`);
  console.log(clipSettings);
  return axios.request({
    url: `${projectUrl}clips/`,
    method: 'POST',
    auth: openshotAuth,
    data: {
      file: fileUrl,
      json: clipSettings.json || {},
      position: clipSettings.position || 0,
      start: clipSettings.start || 0,
      end: clipSettings.end || undefined,
      layer: clipSettings.layer || 0,
      project: projectUrl,
    },
  });
};

export const addClip = (projectUrl: string) => (fileUrl: string, clipSettings): Promise<AxiosResponse<OpenshotClip>> =>
  exports.addClipToProject(projectUrl, fileUrl, clipSettings);
