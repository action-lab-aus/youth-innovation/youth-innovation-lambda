const { addClipToProject } = require("./clips");

/**
 * Given a video, add an audio file as the soundtrack. If the video is longer than the audio clip,
 * loop the audio.
 *
 * @param {string} projectUrl - the url of the project to add the soundtrack to
 * @param {string} file - the object of the file to add as the soundtrack (see the openshot docs for what properties files have)
 * @param {number} lengthOfVideo - the length of the video to add the soundtrack to
 * @returns {Promise<AxiosResponse<object>>} the promise for the request
 */
exports.setSoundtrack = (projectUrl, file, lengthOfVideo) => {
  let remainingTime = lengthOfVideo;
  let tracksAdded = 0;

  const clipPromises = [];

  while (remainingTime > 0) {
    clipPromises.push(
      addClipToProject(projectUrl, file.url, {
        position: tracksAdded * file.json.duration,
        end: Math.min(file.json.duration, remainingTime),
      })
    );
    remainingTime -= file.json.duration;
    tracksAdded++;
  }

  return Promise.all(clipPromises);
};
