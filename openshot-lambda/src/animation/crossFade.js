exports.crossFadeLength = 0.3;

// TODO consider videos shorter than 0.3 seconds

exports.getCrossFade = (videoFPS, videoLength) =>
  videoLength < exports.crossFadeLength
    ? undefined
    : {
        Points: [
          {
            interpolation: 1,
            co: {
              X: 0, // Frame 0
              Y: 0, // Invisible
            },
          },
          {
            interpolation: 1,
            co: {
              X: exports.crossFadeLength * videoFPS + 1, // 0.3 seconds into video
              Y: 1, // Visible
            },
          },
          {
            interpolation: 1,
            co: {
              X: (videoLength - exports.crossFadeLength) * videoFPS + 1, // 0.3 seconds before end of video
              Y: 1, // Visible
            },
          },
          {
            interpolation: 1,
            co: {
              X: videoLength * videoFPS + 1, // Last frame of video
              Y: 0, // Invisible
            },
          },
        ],
      };
