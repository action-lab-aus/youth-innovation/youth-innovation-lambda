import axios, { AxiosResponse } from "axios";
import { OpenshotFile } from "./types";

export const openshotAuth = {
  username: process.env.OPENSHOT_USERNAME,
  password: process.env.OPENSHOT_PASSWORD,
};

export const baseUrl = process.env.OPENSHOT_URL;

/**
 * Openshot's endpoints often return an object with structure:
 * {
 *  count: number,
 *  next: string | null,
 *  previous: string | null,
 *  results: [object]
 * }
 * Where results is the actual data at the endpoint, and next is a url
 * for the next page of responses if there are any (in other words, the
 * response is paginated)
 *
 * This helper function returns all the results concatenated together
 *
 * @param {Promise<AxiosResponse<object>>} data - the response from openshot
 * @param {[object]} accumulatedResponse the results arrays of all the pages concatted together
 */
exports.getAllPages = (
  data: Promise<AxiosResponse<any>>,
  accumulatedResponse: any[]
) =>
  data.then(({ data: response }) => {
    if (response.next != null) {
      return exports.getAllPages(
        axios.request({
          url: response.next,
          method: "GET",
          auth: exports.openshotAuth,
        }),
        accumulatedResponse.concat(response.results)
      );
    }
    return accumulatedResponse.concat(response.results);
  });

/**
 * Get the duration for a given video
 *
 * @param {string} fileUrl - the url of the video to get the duration of
 * @returns {Promise<number>} the length of the video
 */
exports.getVideoLength = (fileUrl: string): Promise<number> => {
  return axios
    .request<OpenshotFile>({
      url: fileUrl,
      method: "GET",
      auth: exports.openshotAuth,
    })
    .then(({ data: response }) => response.json.duration);
};
