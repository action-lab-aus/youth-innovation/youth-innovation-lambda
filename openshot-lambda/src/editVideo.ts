const { default: axios } = require('axios');

const { crossFadeLength, getCrossFade } = require('./animation/crossFade');
import { AxiosResponse } from 'axios';
import { OpenshotClip, OpenshotProject } from './types';
import { baseUrl, openshotAuth } from './util';
const { addFile, createTitle, getFiles } = require('./fileManagement/files');
const { addClipToProject } = require('./clipManagement/clips');
import { addClip } from './clipManagement/clips';
import { new_clip_timing } from './plugins/saveClipTimingsFirebase';
const { setSoundtrack } = require('./clipManagement/soundtrack');

/**
 * Duplicate project with ID projectId
 *
 * @param {Number} projectId - ID of the project to duplicate
 * @param {String} newProjectName - name of the new project
 * @returns {Promise<AxiosResponse<object>>} The promise for the request
 */
const duplicateProject = (projectId: number, newProjectName: string): Promise<AxiosResponse<OpenshotProject>> => {
  return axios.request({
    url: `${baseUrl}/projects/${projectId}/copy/`,
    method: 'POST',
    auth: openshotAuth,
    data: {
      name: newProjectName,
    },
  });
};

/**
 * Given an edit object, create an edit
 *
 * @param {object} editObject - the object representation of the edit:
 * {
 *   projectName {string} - the name of the new project
 *   timeline {[object]} - list of video objects that make up the video
 *   soundtrack {object} - the audio object to use as the soundtrack for the edit
 * }
 *
 * video and audio objects have the format:
 * {
 *   inBaseProject {boolean} - whether the video is in the base project or in the S3 bucket
 *   name {string} - the name of the file
 *   S3Path {string} - the path to the file in the S3 bucket (only required if the file isn't in the base project)
 *   json {object} - an object that specifies extra settings for the clip (see openshot documentation for clip endpoint for details)
 * }
 */
exports.createEditFromObject = async editObject => {
  const { data: project } = await axios.request({
    url: `${baseUrl}/projects/${editObject.project}/`,
    method: 'GET',
    auth: openshotAuth,
  });

  const fps = project.fps_num / project.fps_den;

  /*
   * Here we make an object that has a key for each file to add to the project
   * from the S3 bucket. We use an object so there are no duplicates.
   * The key is the path in the bucket, and the value is the
   * index within the timeline (although we don't use the value)
   */
  console.log('editObject:');
  console.log(editObject);
  const toUpload = editObject.timeline.reduce((acc, el, indx) => {
    if (!el.inBaseProject) {
      acc[el.S3Path] = indx;
    }
    return acc;
  }, {});

  // Add all the files that need to be uploaded
  await Promise.all(Object.entries(toUpload).map(el => addFile(project.url, editObject.bucket, el[0])));

  // If the soundtrack needs to uploaded, upload it
  if (editObject.soundtrack && !editObject.soundtrack.inBaseProject) {
    await addFile(project.url, editObject.bucket, editObject.soundtrack.S3Path);
  }

  // If the overlay needs to be uploaded, uploade it
  if (editObject.overlay && !editObject.overlay.inBaseProject) {
    await addFile(project.url, editObject.bucket, editObject.overlay.S3Path);
  }

  // Get an object that has all of the files in the project
  const assets = await getFiles(project.url);
  const newClip = addClip(project.url);

  // As we add clips to the timeline, we will store promises
  // so we don't have to wait for them one after the other
  let promises = [];

  // When doing cross faeds, we need to
  // layer the next/previous clip under the current
  // clip. So we keep track of the current layer to achieve
  // this
  let currentLayer = 0;

  // The length of the edit so far.
  // (this will increase as we add clips one after the other)
  let currentVideoLength = 0;
  // We memoize the last clip we added so we can read it's length
  let lastAddedClip: OpenshotClip | undefined = undefined;
  // Here we add each clip in the timeline to the edit
  for (const video of editObject.timeline) {
    // Add the clip to the edit
    lastAddedClip = (
      await newClip(assets[video.name].url, {
        layer: currentLayer,
        position: currentVideoLength,
        json: {
          ...video.json,
          alpha: getCrossFade(fps, assets[video.name].json.duration),
        },
      })
    ).data;

    currentLayer++;

    // If necessary, add the text overlay
    if (video.textOverlay) {
      const title = (await createTitle(project, video.textOverlay)).data;
      promises.push(
        newClip(title.url, {
          position: currentVideoLength,
          end: lastAddedClip.end - lastAddedClip.start,
          layer: currentLayer,
          json: {
            alpha: getCrossFade(fps, lastAddedClip.end - lastAddedClip.start),
          },
        }),
      );
      currentLayer++;
    }

    // This is Youth-Innovation specific logic.
    console.log('About to do youth-innovation logic:');
    console.log(video);
    if (editObject.youthInnovation && !video.inBaseProject) {
      await new_clip_timing(editObject.youthInnovation.mediaId, {
        start: currentVideoLength,
        end: currentVideoLength + lastAddedClip.end - lastAddedClip.start - crossFadeLength,
      });
    }

    currentVideoLength += lastAddedClip.end - lastAddedClip.start - crossFadeLength;
  }

  currentVideoLength += crossFadeLength;

  // If appropriate, add the overlay
  if (editObject.overlay) {
    await addClipToProject(project.url, assets[editObject.overlay.name].url, {
      layer: currentLayer,
      position: 0,
      end: currentVideoLength,
      json: {
        alpha: getCrossFade(fps, currentVideoLength),
      },
    });
  }

  // If appropriate, add the soundtrack
  if (editObject.soundtrack) {
    await setSoundtrack(project.url, assets[editObject.soundtrack.name], currentVideoLength);
  }

  // Ensure all clips are added before we export the edit
  await Promise.all(promises);

  const resp = await axios.request({
    url: `${project.url}exports/`,
    method: 'POST',
    auth: openshotAuth,
    data: {
      project: project.url,
      webhook: editObject.webhook,
      json: {
        bucket: editObject.bucket,
        url: editObject.exportPath,
        acl: 'private',
      },
    },
  });

  console.log('Export response:');
  console.log(resp);
};
