export type OpenshotGetResponse<T> = {
  count: number;
  next: string | null;
  previous: string | null;
  results: T[];
};

export type OpenshotFile = {
  url: string;
  id: number;
  media: string;
  project: string;
  actions: string[];
  json: { duration: number };
  date_created: string;
  date_updated: string;
};

export type OpenshotProject = {
  url: string;
  id: number;
  name: string;
  width: number;
  height: number;
  fps_num: number;
  fps_den: number;
  sample_rate: number;
  channels: number;
  channel_layout: number;
  files: string[];
  clips: string[];
  effects: string[];
  exports: string[];
  actions: string[];
  json: { duration: number };
  date_created: string;
  date_updated: string;
};

export type OpenshotClip = {
  url: string;
  file: string;
  id: number;
  position: number;
  start: number;
  end: number;
  layer: number;
  project: string;
  json: {
    scale: number;
    anchor: number;
    channel_filter: OpenshotEffect;
    waveform: boolean;
    scale_x: OpenshotEffect;
    has_video: OpenshotEffect;
    has_audio: OpenshotEffect;
    location_y: OpenshotEffect;
    alpha: OpenshotEffect;
    image: string;
    shear_x: OpenshotEffect;
    shear_y: OpenshotEffect;
    rotation: OpenshotEffect;
    chennel_mapping: OpenshotEffect;
    gravity: number;
    scale_y: OpenshotEffect;
    volume: OpenshotEffect;
    title: string;
    wave_color: {
      blue: OpenshotEffect;
      alpha: OpenshotEffect;
      green: OpenshotEffect;
      red: OpenshotEffect;
    };
    display: number;
    time: OpenshotEffect;
    location_x: OpenshotEffect;
  };
  date_created: string;
  date_updated: string;
};

export type OpenshotEffect = {
  Points: {
    co: Coord;
    handle_type?: number;
    handle_right?: Coord;
    handle_left?: Coord;
    interpolation: number;
  }[];
};

export type Coord = { X: number; Y: number };
