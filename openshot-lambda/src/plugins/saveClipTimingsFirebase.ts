import admin, { ServiceAccount } from 'firebase-admin';

// var serviceAccount = require('./firebase-config.json');
import * as serviceAccount from '../../firebase-config.json';

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount as ServiceAccount),
});

const db = admin.firestore();

type Clip = { start: number; end: number };

export const new_clip_timing = async (mediaId: string, clip: Clip) => {
  const mediaRef = db.collection('media').doc(mediaId);
  const media = (await mediaRef.get()).data();
  console.log(`Saving clip to fb: ${mediaId}`);
  console.log(`${media}`);
  console.log(`${mediaRef}`);
  mediaRef.set({ ...media, clips: media.clips ? [...media.clips, clip] : [clip] });
};
