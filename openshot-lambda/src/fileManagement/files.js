const path = require("path");
const { getAllPages, baseUrl, openshotAuth } = require("../util");
const { default: axios } = require("axios");

/**
 * Add S3 file to Project
 *
 * @param {string} projectUrl - url to the project the file is being added to
 * @param {string} bucket - the aws bucket to get the file from
 * @param {string} awsUrl - AWS Path to the file
 * @returns {Promise<AxiosResponse<object>>} the promise for the request
 */
exports.addFile = (projectUrl, bucket, awsUrl) => {
  return axios.request({
    url: `${baseUrl}/files/`,
    method: "POST",
    auth: openshotAuth,
    data: {
      media: null,
      project: projectUrl,
      json: JSON.stringify({
        url: awsUrl,
        bucket: bucket,
      }),
    },
  });
};

const validTemplate = (template) => {
  return (
    template == "Center-Text.svg" ||
    template == "Bottom-Text.svg" ||
    template == "Top-Text.svg" ||
    template == "Bottom-Left.svg"
  );
};

/**
 * Generate a title for a given project with the given text
 * Valid templates are "Center-Text.svg", "Bottom-Text.svg"
 * and "Top-Text.svg"
 *
 * @param {object} projectObject - the project object
 * @param {object} titleObject - the object that has the settings for the title
 * @returns {Promise<AxiosResponse<object>>} the promise for the request
 */
exports.createTitle = (projectObject, titleObject) => {
  if (!validTemplate(titleObject.template)) {
    console.log(`Invalid template passed to createTitle: ${titleObject.template}`);
    throw new Error();
  }
  return axios.request({
    url: `${projectObject.url}title/`,
    auth: openshotAuth,
    method: "POST",
    data: {
      ...titleObject,
      text: titleObject.text ?? projectObject.name,
    },
  });
};

/**
 * List the file obejcts for a given project
 *
 * @param {string} projectUrl - the url of the project to list the files of
 * @returns {Promise<[object]>} the list of the project's files
 */
exports.listProjectFiles = (projectUrl) => {
  return getAllPages(
    axios.request({
      url: `${projectUrl}files/`,
      methd: "GET",
      auth: openshotAuth,
    }),
    []
  );
};

/**
 * Get all the file objects for a given project
 *
 * @param {string} projectUrl - the url of the project to get the files of
 * @returns {object} an object that has a key for each file in the project, whose value
 * is that file's object as defined by openshot. The key is the name of the file. (see
 * openshots documentation for the structure of the file objects)
 */
exports.getFiles = (projectUrl) => {
  return exports.listProjectFiles(projectUrl).then((results) => {
    console.log("All files in project:");
    console.log(results);
    return results.reduce((obj, el) => {
      obj[path.basename(el.json.path)] = el;
      return obj;
    }, {});
  });
};
