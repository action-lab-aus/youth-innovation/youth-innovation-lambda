# OpenShot Lambda

This project acts as a middle man between a client application and ActionLab's instance of the Openshot Web API.

The main purpose of this project is to allow client code to describe video edits as a sequence of source files that play one after the other, rather than in terms of absolute timestamp values.

## Building, Testing and Deploying

Building this project requires typescript to be installed. To continuously build the project, from the 'openshot-lambda' directory:

```
tsc -w
```

This will create the 'build' directory for deployment.

Deploying this project requires the installation of the AWS CLI.

To deploy to AWS Lambda, from the 'openshot-lambda' directory,

```
zip -r function.zip . && aws lambda update-function-code --function-name YouthInnovation --zip-file fileb://./function.zip
```

From the AWS Lambda console, the following environment variables should be set:

- SECRET (a secret key that is required to use the endpoint)
- OPENSHOT_URL (the url of the Openshot instance)
- OPENSHOT_USERNAME (the username for signing calls to the Openshot instance)
- OPENSHOT_PASSWORD (the password for signing calls to the Openshot instance)

## API

Endpoint: https://4dt4hvw8b5.execute-api.ap-southeast-2.amazonaws.com/default/YouthInnovation

Allows: POST

Expects:

```JSON
{
  "key": "Your Secret key",
  "project": "The ID of the project to edit/treat as the base project (this project shouldn't have any clips at the start of the edit)",
  "bucket": "The name of the bucket to fetch files to be uploaded from",
  "webhook": "Url to make POST request to when the video has completed editing and exporting",
  "exportPath": "The S3 path to put the export at",
  "timeline": "An array of clip objects that specify what videos should be in the edit",
  "overlay": "An object that specifies an image to overlay over the entire video",
  "soundtrack": "An object that specifies what audio track should be used as the soundtrack"
}
```

Clip Objects

```JSON
{
  "inBaseProject": "boolean for whether the asset is in the base project or S3",
  "S3Path": "path within the S3 bucket to the asset, if the asset isn't in the base project",
  "name": "the name of the asset. If the asset isn't in the base project, it should be the same as the name in the S3 instance",
  "textOverlay": "text overlay object for this clip"
}
```

Text Overlay Objects

```JSON
{
  "template": "the template to use. Currently available templates: 'Center-Text.svg', 'Top-Text.svg', 'Bottom-Text.svg'",
  "text": "optional. This is the text to put in the title. If it's omitted, the name of the project will be used",
  "font_size": "optional. number. The font size for the text",
  "font_name": "optional. font to use for the text. See the openshot docs for a list of available fonts",
  "fill_color": "optional. color for text fill",
  "fill_opacity": "optional. number. Text fill opacity",
  "stroke_color": "optional. color for text stroke",
  "stroke_size": "optional. number. The size of the stroke",
  "stroke_opacity": "optional. number. The opacity of the stroke",
  "drop_shadow": "optional. boolean. Wether the text should have a dop shadoe",
  "background_color": "optional. color of the background",
  "background_opacity": "optional. number. The opacity of the background"
}
```

Example:

```JSON
{
  "projectName": "Some Name",
  "key": "key",
  "webhook": "https://us-central1-youthinnovation-firebase.cloudfunctions.net/downloadEdit?project=${change.after.id}&phase=${newData.phase}",
  "baseProject": 91,
  "bucket": "youth-innovation",
  "exportPath": "submissions/edited.mp4",
  "overlay": {
    "inBaseProject": true,
    "name": "glass.png"
  },
  "timeline": [
    {
      "inBaseProject": true,
      "name": "Intro.mp4",
      "textOverlay": {
        "text": "Intro Text",
        "template": "Center-Text.svg"
      }
    },
    {
      "inBaseProject": false,
      "name": "Asset Name.mp4",
      "S3Path": "submissions/assets/Asset Name.mp4",
    },
    {
      "inBaseProject": true,
      "name": "Intro.mp4",
      "json": "{\"scale\":0}"
    }
  ],
  "soundtrack": {
    "inBaseProject": true,
    "name": "sax.mp3",
  }
}
```
