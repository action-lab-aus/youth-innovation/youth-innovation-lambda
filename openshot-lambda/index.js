const editVideo = require('./build/src/editVideo');

exports.handler = async event => {
  const body = event.body ? JSON.parse(event.body) : { projectName: 'No Name Provided' };

  if (body.key !== process.env.SECRET) return { statusCode: 401 };

  try {
    // await editVideo.createEdit(body.projectName, body.videoUrl);
    await editVideo.createEditFromObject(body);
  } catch (error) {
    console.log('Error creating edit:');
    console.log(error);
  }

  const response = {
    statusCode: 200,
    body: JSON.stringify({
      message: 'Hello world',
    }),
  };
  return response;
};
