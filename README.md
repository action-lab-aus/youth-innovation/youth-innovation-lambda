# Limitless Lambda

This repository contains source code for the AWS Lambda Functions used in the Limitless Global competition infrastructure.

See the child directories `openshot-lambda` & `submission-zip-lambda` for details about the individual functions.
